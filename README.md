# Face Generation

The goal of this project is to define and train a DCGAN on a dataset of faces, to get a generator network that can generate *new* images of faces that look as realistic as possible!

The project will be broken down into a series of tasks from **loading in data to defining and training adversarial networks**. At the end of the notebook, we'll be able to visualize the results of our trained Generator to see how it performs.

## Overall steps

The project consists of the following steps:

1. Get the data
   - Preprocessed data
2. Visualize the CelebA data
   - Preprocess and load the data
3. Create a DataLoader
   - Create a DataLoader
4. Scale the data to a pixel range of -1 to 1
5. Define the Model
   - Discriminator
   - Generator
6. Initialize the weights of your networks
7. Build complete network
8. Discriminator and Generator Losses
   - Discriminator loss
   - Generator loss
9. Optimizers
10. Training
11. Visualize losses
12. Generate new samples
13. Observations


## Get the data

We will be using the [CelebFaces Attributes Dataset (CelebA)](http://mmlab.ie.cuhk.edu.hk/projects/CelebA.html) to train your adversarial networks.

This dataset is more complex than the number datasets (like MNIST or SVHN) we've been working with, and so, we should prepare to define deeper networks and train them for a longer time to get good results. It is suggested that we utilize a GPU for training.

### Preprocessed data

Since the project's main focus is on building the GANs, *some* of the pre-processing has already been done by Udacity. Each of the CelebA images has been cropped to remove parts of the image that don't include a face, then resized down to 64x64x3 NumPy images. Some sample data is show below.

<img src='figures/processed_face_data.png' width=60% />

> If we are working locally, you can download this data [by clicking here](https://s3.amazonaws.com/video.udacity-data.com/topher/2018/November/5be7eb6f_processed-celeba-small/processed-celeba-small.zip)

This is a zip file that we'll need to extract in the home directory of this notebook for further loading and processing. After extracting the data, you should be left with a directory of data `processed_celeba_small/`

## **Repository** ##

The original Udacity project instructions can be found [here](https://github.com/udacity/deep-learning-v2-pytorch/tree/master/project-face-generation).


## **Dependencies** ##

The information regarding dependencies for the project can be obtained from [here](https://github.com/udacity/deep-learning-v2-pytorch).

## **Detailed Writeup** ##

We studied the impact on the performance on the quality of generated images of the following:
1. image sizes   - images of size 64x64x3 reduced to 32x32x3 and increased to 128x128x3
2. normalization - batch normalization, instance normalization, and group normalization 
3. loss function - RMSE loss and cross-entropy loss.

Detailed report can be found in the following:

1. [Batch norm, size 32x32x3, CE](./dcgan_CE_loss_BN_32.ipynb).
2. [Batch norm, size 32x32x3, RMSE](./dcgan_rmse_loss_BN_32.ipynb).
3. [Instance norm, size 32x32x3, CE](./dcgan_CE_loss_IN_32.ipynb).
4. [Instance norm, size 32x32x3, RMSE](./dcgan_rmse_loss_IN_32.ipynb).
5. [Group norm, size 32x32x3, CE](./dcgan_CE_loss_GN_32.ipynb).
6. [Group norm, size 32x32x3, RMSE](./dcgan_rmse_loss_GN_32.ipynb).
7. [Batch norm, size 128x128x3, CE](./dcgan_CE_loss_BN_128.ipynb).
8. [Batch norm, size 128x128x3, RMSE](./dcgan_rmse_loss_BN_128.ipynb).
9. [Instance norm, size 128x128x3, CE](./dcgan_CE_loss_IN_128.ipynb).
10. [Instance norm, size 128x128x3, RMSE](./dcgan_rmse_loss_IN_128.ipynb).
11. [Group norm, size 128x128x3, CE](./dcgan_CE_loss_GN_128.ipynb).
12. [Group norm, size 128x128x3, RMSE](./dcgan_rmse_loss_GN_128.ipynb).

## **Acknowledgments** ##

I would like to thank Udacity for giving me this opportunity to work on an awesome project. 

